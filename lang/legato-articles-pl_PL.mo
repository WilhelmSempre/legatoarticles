��    1      �  C   ,      8  
   9     D  1   P     �  '   �  *   �     �  "   �  8     $   F     k     ~     �  D   �     �               $     -  1   C     u     |     �  2   �  1   �          /     C     J     Y     i     q     �  �   �  	             -     2     ;     I     U     [     b     q       	   �     �  %   �  f  �     :
     I
  J   X
     �
  >   �
  I   �
     4  1   E  P   w     �     �     �  B     G   S     �     �     �     �     �  O   �     C      J     k  0   �  0   �     �     �          #     4     J     X     v  �   �     u     }     �     �  
   �     �     �     �  	   �     �     �     �  ]     )   p                                  .      (                               ,   -                "   	      #   &                              1       *              $   /       0   '   +                           !       
      )          %    Add advert Add article Adding photos, audio and video links is possible. Advert Advert content on homepage and article: Advert content on homepage and in article: Advert widget Advert widget to show post adverts Article has bee sent to administrators for verification. Article has been added successfully! Articles category: Articles per page: Before adding read rules. Before article publishing it has been <b>sent to administrators.</b> Characters left: Choose category: Confirm password: Content: Do not show in future During publishing you will be informed by e-mail. Email: Example: http://yoursite.pl Field data does not match Field length must not be longer than %s characters Field length must not be lower than %s characters Fields are not the same Fill required field Filter Incorrect mail Incorrect value Log in! Login or email: New article On homepage will be showed 3 lines (180 characters). More after link clicking. Characters limitation is in bottom left border of form. Password: Post published Save Sign up! Site address: Site title: Tags: Title: Understand it! User created! User: Username: Write news with one of topic: You do not have account yet? Sign up! Project-Id-Version: Legato Articles
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: Rafał Głuszak <rafal.gluszak@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
Language: pl_PL
X-Poedit-SearchPath-0: .
 Dodaj reklamę Dodaj artykuł Istnieje możliwość dodania do artykułu zdjęć, linków audio i video. Reklama Treść reklamy wyświetlana na stronie głównej i artykułu: Treść reklamy wyświetlana na stronie głównej i na stronie artykułu: Widget reklamowy Widget reklamowy pokazuje wszystkie reklamy posta Artykuł został przesłany do moderacji w celu weryfikacji przez administracje. Artykuł został dodany! Kategorie artykułów: Artykułów na stronie: Przed przesłaniem artykułu prosimy zapoznać się z regulaminem. Artykuł przed opublikowaniem, <b>zostanie przesłany do moderacji.</b> Pozostało znaków: Wybierz kategorie: Potwierdź hasło: Treść: Nie pokazuj ponownie O jego zaakceptowaniu zgłaszający zostanie natychmiast poinformowany emailem. Email: Przykład: http://twojastrona.pl Dane pola nie zgadzają się Długość powinna być większa niż %s znaków Długość powinna być mniejsza niż %s znaków Pola nie są te same Wypełnij wymaganą wartość Filtr Niepoprawny mail Niepoprawna wartość Zaloguj się! Nazwa użytkownika lub email: Nowy artykuł Na stronie głównej, oprócz tytułu, zostaną opublikowane 3 pierwsze wiersze (180 znaków). Dalsza część artykułu dostępna jest pokliknięciu w link na stronie głównej. Limit znaków jest pokazywany w lewym dolnym rogu formularza. Hasło: Post opublikowany Zapisz Zarejestruj się! Adres WWW: Tytuł strony: Tagi: Tytuł: Rozumiem! Użytkownik utworzony! Użytkownik: Nazwa użytkownika: Podziel się z użytkownikami najświeższymi wiadomościami na jeden z poniższych tematów: Nie masz jeszcze konta? Zarejestruj się! 