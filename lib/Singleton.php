<?php


/**
 * Class Singleton
 */
abstract class Singleton
{

    /**
     * @var null
     */
    private static $instance = null;

    /**
     * Singleton constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return null
     */
    public static function getInstance()
    {
        $classname = get_called_class();

        if (!self::$instance instanceof $classname) {
            self::$instance = new $classname();
        }

        return self::$instance;
    }
}