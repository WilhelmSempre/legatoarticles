<?php


/**
 * Class UserManager
 */
class UserManager
{

    /**
     * @param array $user
     * @return int|WP_Error
     */
    public function addUser($user = [])
    {

        $createdUser = wp_insert_user($user);

        if (!$createdUser instanceof WP_Error) {

            $language = get_locale();

            $mail = file_get_contents(PLUGIN_MAILS_DIR . $language . '/_register-user-mail.html');

            $mailMessage = strtr($mail, [
                '{{ site }}' => get_bloginfo('name'),
                '{{ username }}' => $user['user_login']
            ]);

            wp_mail($user['user_email'], __('New user registration', 'legato-bank'), $mailMessage);

        }

        return $createdUser;
    }

    /**
     * @param array $options
     * @return array|bool
     */
    public function getUser($options = [])
    {

        $defaultOptions = [
            'login' => '',
            'id' => 0,
            'email' => ''
        ];

        $options = array_merge($defaultOptions, $options);

        /** @var WP_User $userData */
        $userData = null;

        if ($options['login'] !== '') {

            $userData = get_users([
                'search' => $options['login']
            ]);

        } else if ($options['id'] !== 0) {

            $userData = get_userdata($options['id']);
        } else if ($options['email'] !== '') {

            $userData = get_users([
                'user_email' => $options['email']
            ]);
        } else {
            return false;
        }

        if (!$userData) {
            return false;
        }

        return [
            'login' => $userData[0]->data->user_login,
            'email' => $userData[0]->data->user_email,
            'id' => $userData[0]->data->ID,
            'password' => $userData[0]->data->user_pass
        ];
    }

    /**
     * @param $login
     * @return bool
     */
    public function ifUserExists($login) {
        return !username_exists($login) ? false : true;
    }

    /**
     * @return bool
     */
    public function ifUserLogged()
    {
        return is_user_logged_in();
    }
}