<?php

class PerpageManager
{

    /**
     * @var
     */
    private $perpageValue;

    /**
     * @var int
     */
    private $defaultPerpageValue = 5;

    /**
     * @var CookierManager
     */
    private $cookier;

    /**
     * PerpageManager constructor.
     */
    public function __construct()
    {
        $this->cookier = new CookierManager();

        if (!$this->ifPerpageCookiesExists()) {

            $perpageValue = $this->defaultPerpageValue;
            $this->cookier->add('legato_perpage_value', $perpageValue);
        } else {

            $perpageValue = $this->cookier->get('legato_perpage_value');
        }

        $this->setPerpageValue($perpageValue);
    }

    /**
     * @param $perpage
     * @return $this
     */
    public function setPerpageValue($perpage)
    {
        $this->perpageValue = $perpage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerpageValue()
    {
        return (int) $this->perpageValue;
    }

    /**
     * @param $perpageValue
     * @return $this
     */
    public function savePerpageValueToCookie($perpageValue = 5)
    {

        $this->cookier->update('legato_perpage_value', $perpageValue);

        $this->setPerpageValue($perpageValue);

        return $this;
    }

    /**
     * @return bool
     */
    public function ifPerpageCookiesExists()
    {
        return $this->cookier->exists('legato_perpage_value');
    }
}