<?php

/**
 * Class ValidationManager
 */
final class ValidationManager
{
    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var array
     */
    private $criterium = [];

    /**
     * @var array
     */
    private $fields = [];

    /**
     * @param array $fields
     * @return $this
     */
    public function setFields($fields = [])
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param array $criterium
     * @return $this
     */
    public function setCriterium($criterium = [])
    {
        $this->criterium = $criterium;
        return $this;
    }

    /**
     * @param array $errors
     * @return $this
     */
    public function setErrors($errors = [])
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @param null $key
     * @return array
     */
    public function getErrors($key = null)
    {
        if ($key) {
            return $this->errors[$key][0];
        }

        return $this->errors;
    }

    /**
     * @param null $key
     * @return bool
     */
    public function isError($key = null) {
        if ($key) {
            return isset($this->errors[$key]);
        }

        return count($this->errors) > 0;
    }

    /**
     * @return array
     */
    private function createCriteriumFromArray()
    {
        $criteriumPieces = [];

        foreach ($this->criterium as $key => $criterium) {
            $criteriumPieces[$key] = explode('|', $criterium);
            foreach ($criteriumPieces[$key] as $index => $piece) {
                if (strpos($piece, ':') !== false) {
                    $criteriumPieces[$key][] = explode(':', $piece);
                    unset($criteriumPieces[$key][$index]);
                }
            }
        }

        return $criteriumPieces;
    }

    /**
     * @param $fieldValue
     * @return bool
     */
    private function checkRequired($fieldValue)
    {
        return $fieldValue !== '';
    }


    /**
     * @param $fieldValue
     * @return bool|int
     */
    private function checkAlpha($fieldValue)
    {
        if ($fieldValue !== '') {
            return preg_match('/^[\s\p{L}]+$/u', $fieldValue);
        }

        return true;
    }

    /**
     * @param $fieldValue
     * @return bool|int
     */
    private function checkAlphaNum($fieldValue)
    {
        if ($fieldValue !== '') {
            return preg_match('/^[\s\p{L}0-9]+$/u', $fieldValue);
        }

        return true;
    }

    /**
     * @param $fieldValue
     * @return bool|int
     */
    private function checkNum($fieldValue)
    {
        if ($fieldValue !== '') {
            return preg_match('/^[0-9]+$/u', $fieldValue);
        }

        return true;
    }

    /**
     * @param $fieldValue
     * @return bool|mixed
     */
    private function checkEmail($fieldValue)
    {
        if ($fieldValue !== '') {
            return filter_var($fieldValue, FILTER_VALIDATE_EMAIL);
        }

        return true;
    }

    /**
     * @param $fieldValue
     * @param $max
     * @return bool
     */
    private function checkMax($fieldValue, $max)
    {
        if ($fieldValue !== '') {
            return strlen($fieldValue) < $max;
        }

        return true;
    }

    /**
     * @param $fieldValue
     * @param $min
     * @return bool
     */
    private function checkMin($fieldValue, $min)
    {
        if ($fieldValue !== '') {
            return strlen($fieldValue) > $min;
        }

        return true;
    }

    /**
     * @param $fieldValue
     * @param $compare
     * @return bool
     */
    private function checkCompare($fieldValue, $compare)
    {
        if ($fieldValue !== '') {
            return $fieldValue === $compare;
        }

        return true;
    }

    /**
     * @param $fieldValue
     * @param $pattern
     * @return bool|int
     */
    private function checkMatch($fieldValue, $pattern)
    {
        if ($fieldValue !== '') {
            var_dump($pattern);
            return preg_match($pattern, $fieldValue);
        }

        return true;
    }

    /**
     * @throws Exception
     */
    public function validate()
    {
        $criteriumKeys = array_keys($this->criterium);
        $fieldsKeys = array_keys($this->fields);

        if (count(array_diff($criteriumKeys, $fieldsKeys)) > 0) {
            throw new \Exception('Criterium and fields values does not match!');
        }

        $fieldsWithCriterias = $this->createCriteriumFromArray();

        foreach ($fieldsWithCriterias as $key => $fields) {
            foreach ($fields as $criterias) {
                if (is_array($criterias)) {
                    switch ($criterias[0]) {
                        case 'max' :
                            if (!$this->checkMax($this->fields[$key], $criterias[1])) {
                                $this->errors[$key][] = sprintf(__('Field length must not be lower than %s characters', 'legato-articles'), $criterias[1]);
                            }
                        break;
                        case 'min' :
                            if (!$this->checkMin($this->fields[$key], $criterias[1])) {
                                $this->errors[$key][] = sprintf(__('Field length must not be longer than %s characters', 'legato-articles'), $criterias[1]);
                            }
                        break;
                        case 'match' :
                            if (!$this->checkMatch($this->fields[$key], $criterias[1])) {
                                $this->errors[$key][] = __('Field data does not match', 'legato-articles');
                            }
                        break;
                        case 'compare' :
                            if (!$this->checkCompare($this->fields[$key], $criterias[1])) {
                                $this->errors[$key][] = __('Fields are not the same', 'legato-articles');
                            }
                            break;
                    }
                } else {
                    switch ($criterias) {
                        case 'required' :
                            if (!$this->checkRequired($this->fields[$key])) {
                                $this->errors[$key][] = __('Fill required field', 'legato-articles');
                            }
                        break;
                        case 'alpha' :
                            if (!$this->checkAlpha($this->fields[$key])) {
                                $this->errors[$key][] = __('Incorrect value', 'legato-articles');
                            }
                        break;
                        case 'alpha_num' :
                            if (!$this->checkAlphaNum($this->fields[$key])) {
                                $this->errors[$key][] = __('Incorrect value', 'legato-articles');
                            }
                        break;
                        case 'num' :
                            if (!$this->checkNum($this->fields[$key])) {
                                $this->errors[$key][] = __('Incorrect value', 'legato-articles');
                            }
                        break;
                        case 'email' :
                            if (!$this->checkEmail($this->fields[$key])) {
                                $this->errors[$key][] = __('Incorrect email', 'legato-articles');
                            }
                        break;
                    }
                }
            }
        }
    }
}