<?php

/**
 * Class PagerManager
 */
class PagerManager
{

    /**
     * @param array $options
     * @return int|WP_Error
     */
    public function addPage($options = [])
    {
        return wp_insert_post($options);
    }

    /**
     * @param $slug
     * @return array|false|WP_Post
     */
    public function removePage($slug)
    {
        $page = get_page_by_path($slug, OBJECT, 'page');
        $pageID = (!$page) ? 0 : $page->ID;

        return wp_delete_post($pageID, true);
    }

    /**
     * @param $slug
     * @return false|string
     */
    public function getPageAddress($slug)
    {
        $page = get_page_by_path($slug, OBJECT, 'page');
        $pageID = (!$page) ? 0 : $page->ID;

        return get_permalink($pageID);
    }
}