<?php

/**
 * Class ArticleManager
 */
class ArticleManager
{

    /**
     * @var
     */
    private $postQuery;

    /**
     * @param array $article
     * @return $this
     */
    private function addTags($article = [])
    {
        wp_set_post_tags($article['id'], $article['tags'], true);
        return $this;
    }

    /**
     * @param array $article
     * @return array
     */
    private function getTags($article = [])
    {
        $tags = get_the_tags($article['id']);
        $tagsArray = [];

        if ($tags) {

            /** @var WP_Term $tag */
            foreach ($tags as $tag) {
                $tagsArray[$tag->name] = get_tag_link($tag->term_id);
            }
        }

        $article['tags'] = $tagsArray;

        return $article;
    }

    /**
     * @param array $article
     * @return $this
     */
    private function addAdvert($article = [])
    {
        if (isset($article['advert'])) {
            add_post_meta($article['id'], 'advert', $article['advert'], true);
            add_post_meta($article['id'], 'advert_title', $article['advert-title'], true);
            add_post_meta($article['id'], 'advert_address', $article['advert-address'], true);
        }

        return $this;
    }

    /**
     * @param array $article
     * @return array
     */
    private function getCategories($article = [])
    {
        $categories = get_the_category($article['id']);
        $categoriesArray = [];

        /** @var WP_Term $category */
        foreach ($categories as $category) {
            $categoriesArray[$category->term_id] = $category->name;
        }

        $article['category'] = $categoriesArray;

        return $article;
    }

    /**
     * @param array $article
     * @return array
     */
    private function getAdvert($article = [])
    {
        $advert = get_post_meta($article['id'], 'advert', true);

        if ($advert) {
            $advertTitle = get_post_meta($article['id'], 'advert_title', true);
            $advertAddress = get_post_meta($article['id'], 'advert_address', true);

            $article['advert'] = $advert;
            $article['advert-title'] = $advertTitle;
            $article['advert-address'] = $advertAddress;
        }

        return $article;
    }

    /**
     * @param array $article
     * @return bool
     */
    public function addArticle($article = [])
    {

        $options = [
            'post_title' => $article['title'],
            'post_name' => str_replace(' ', '-', $article['title']),
            'post_status' => 'pending',
            'post_content' => $article['content'],
            'post_author' => $article['author'],
            'post_category' => [$article['category']]
        ];

        $postID = wp_insert_post($options);

        if (!$postID) {
            return false;
        }

        $article['id'] = $postID;

        $this->addAdvert($article)
            ->addTags($article);

        return true;
    }

    /**
     * @param array $options
     * @return array
     */
    public function getArticle($options = [])
    {

        if (!array_key_exists('p', $options)) {

            $defaults = [
                'posts_per_page' => 5,
                'cat' => 0, 'orderby' => 'date',
                'offset' => 0,
                'order' => 'DESC', 'include' => [],
                'exclude' => [], 'meta_key' => '',
                'meta_value' =>'', 'post_type' => 'post',
                'suppress_filters' => true,
                'post_status' => 'publish'
            ];

            $options = array_merge($defaults, $options);
        }

        $query = new WP_Query($options);
        $articles = [];

        $this->setPostQuery($query);

        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();

                $articles[get_the_ID()] = [
                    'title' => get_the_title(),
                    'id' => get_the_ID(),
                    'content' => get_the_content(),
                    'author' => get_the_author_meta('ID')
                ];

                $articles[get_the_ID()] = $this->getTags($articles[get_the_ID()]);
                $articles[get_the_ID()] = $this->getAdvert($articles[get_the_ID()]);
                $articles[get_the_ID()] = $this->getCategories($articles[get_the_ID()]);
            }

            wp_reset_postdata();
        }

        return $articles;
    }

    /**
     * @return mixed
     */
    public function getPostQuery()
    {
        return $this->postQuery;
    }

    /**
     * @param WP_Query $query
     * @return $this
     */
    private function setPostQuery(WP_Query $query)
    {
        $this->postQuery = $query;
        return $this;
    }
}