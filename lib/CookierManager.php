<?php

/**
 * Class CookierManager
 */
class CookierManager
{

    /**
     * @param $name
     * @param $content
     * @param int $time
     * @return $this
     */
    public function add($name, $content, $time = 60 * 60 * 60 * 24)
    {
        setcookie($name, $content, time() + $time, COOKIEPATH, COOKIE_DOMAIN);
        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public function remove($name)
    {
        setcookie($name, '', -1, COOKIEPATH, COOKIE_DOMAIN);
        return $this;
    }

    /**
     * @param $name
     * @param $content
     * @return $this
     */
    public function update($name, $content)
    {
        $this->remove($name)
            ->add($name, $content);

        return $this;
    }

    /**
     * @param $name
     * @return bool
     */
    public function get($name)
    {
        return isset($_COOKIE[$name]) ? $_COOKIE[$name] : false;
    }

    /**
     * @param $name
     * @return bool
     */
    public function exists($name)
    {
        return isset($_COOKIE[$name]);
    }
}