<?php


/**
 * Class PaginationManager
 */
class PaginationManager
{

    private $paged;

    /**
     * @param WP_Query $query
     */
    public function generate(WP_Query $query)
    {

        if (is_singular())
            return;

        if ($query->max_num_pages <= 1)
            return;

        $this->paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
        $max = intval($query->max_num_pages);

        if ($this->paged >= 1)
            $links[] = $this->paged;

        if ($this->paged >= 3) {
            $links[] = $this->paged - 1;
            $links[] = $this->paged - 2;
        }

        if (($this->paged + 2) <= $max) {
            $links[] = $this->paged + 2;
            $links[] = $this->paged + 1;
        }

        echo '<nav aria-label="Page navigation"><ul class="pagination">' . "\n";

        if (get_previous_posts_link())
            printf('<li>%s</li>' . "\n", get_previous_posts_link());

        if (!in_array(1, $links)) {
            $class = 1 == $this->paged ? ' class="active"' : '';

            printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');

            if (!in_array(2, $links))
                echo '<li><a>…</a></li>';
        }

        sort($links);
        foreach ((array)$links as $link) {
            $class = $this->paged == $link ? ' class="active"' : '';
            printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
        }

        if (!in_array($max, $links)) {
            if (!in_array($max - 1, $links))
                echo '<li><a>…</a></li>' . "\n";

            $class = $this->paged == $max ? ' class="active"' : '';
            printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max)), $max);
        }

        if (get_next_posts_link() && $this->paged < $max)
            printf('<li>%s</li>' . "\n", get_next_posts_link());

        echo '</ul></nav>' . "\n";

    }

    /**
     * @return int|mixed
     */
    public function getCurrentPage()
    {
        return get_query_var('paged') ? get_query_var('paged') : 1;
    }
}