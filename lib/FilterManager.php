<?php

/**
 * Class FilterManager
 */
class FilterManager
{

    /**
     * @var array
     */
    private $filteredCategories = '';

    /**
     * @var CookierManager
     */
    private $cookier;

    /**
     * @var int
     */
    private $defaultCategory = 0;

    /**
     * FilterManager constructor.
     */
    public function __construct()
    {
        $this->cookier = new CookierManager();

        if (!$this->ifFilterCookiesExists()) {

            $filteredCategories = $this->defaultCategory;
            $this->cookier->add('legato_filtered_category_value', $filteredCategories);
        } else {
            $filteredCategories = $this->cookier->get('legato_filtered_category_value');
        }

        $this->setFilteredCategories($filteredCategories);
    }

    /**
     * @param string $filteredCategories
     * @return $this
     */
    public function setFilteredCategories($filteredCategories = '')
    {
        $this->filteredCategories = $filteredCategories;
        return $this;
    }

    /**
     * @return array
     */
    public function getFilterCategories()
    {
        return $this->filteredCategories;
    }

    /**
     * @param $filteredCategories
     * @return $this
     */
    public function saveFilteredCategoriesToCookie($filteredCategories = 0)
    {

        if ($filteredCategories === 0) {
            $this->cookier->remove('legato_filtered_category_value');

            return $this;
        }

        $this->cookier->update('legato_filtered_category_value', $filteredCategories);
        $this->setFilteredCategories($filteredCategories);

        return $this;
    }


    /**
     * @return bool
     */
    public function ifFilterCookiesExists()
    {
        return $this->cookier->exists('legato_filtered_category_value');
    }

    /**
     * @return bool
     */
    public function ifFilterCategoriesExists()
    {
        return (int) $this->filteredCategories[0] !== 0;
    }
}