<?php

/**
 * Class AdvertWidget
 */
class AdvertWidget extends WP_Widget {

    /**
     * WhoAreMe constructor.
     */
    public function __construct() {
        $widget_ops = [
            'classname' => 'advert-item',
            'description' => __('Advert widget to show post adverts', 'legato-bank'),
        ];
        parent::__construct('advert_widget', __('Advert widget'), $widget_ops);
    }

    /**
     * @param array $args
     * @param array $instance
     * @return $this
     */
    public function widget($args, $instance) {
        ?>
        <?php
        if (is_single()) {

            $articleManager = new ArticleManager();

            $options = [
                'p' => get_the_ID()
            ];

            $article = $articleManager->getArticle($options);

            if (count($article) > 0 && isset($article[get_the_ID()])) {
                if (isset($article[get_the_ID()]['advert-address'])) {
                    echo $args['before_widget'];
                    ?>
                        <aside class="advert-sidebar-content col-xs-12">
                            <h3><?php echo __('Advert', 'legato-bank'); ?></h3>
                                <p class="advert-sidebar-content-author"><b><?php echo get_the_author_meta('nicename', $article[get_the_ID()]['author']); ?></b></p>
                                <a target="_blank" class="advert-sidebar-content-meta-advert" href="<?php echo $article[get_the_ID()]['advert-address']; ?>">
                                   <strong><?php echo $article[get_the_ID()]['advert-title'] !== '' ? $article[get_the_ID()]['advert-title'] : $article[get_the_ID()]['advert-address'] ?></strong></a>
                        </aside>
                    <?php echo $args['after_widget'];
                }
            }
        }
        return $this;
    }

    /**
     * @param array $instance
     * @return $this
     */
    public function form($instance) {
        return $this;
    }

    /**
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update($new_instance, $old_instance) {
        $instance = [];
        return $instance;
    }
}