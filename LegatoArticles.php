<?php

/*
Plugin Name: Legato Articles
Version: 1.0
Description: Dodaj post z poziomu panelu użytkownika! Szybko i sprawnie!<br> <strong>Wymagany iFRAME i WP Autoloader!</strong>
Author: Rafał Głuszak
Author URI: http://www.wilhelmsempre.pl/
Text Domain: legato-articles
*/

if (!class_exists('LegatoArticles')) {

    require_once 'lib/Singleton.php';

    /**
     * Class LegatoArticles
     */
    class LegatoArticles extends Singleton
    {
        /**
         * @return $this
         */
        private function install()
        {
            add_option('legato_options', []);

            return $this;
        }

        /**
         * @return $this
         */
        private function addPages()
        {
            $options = [
                'post_title' => __('Add article', 'legato-articles'),
                'post_name' => 'add-article',
                'post_type' => 'page',
                'post_status' => 'publish',
                'post_content' => '[legato-add-article-form]'
            ];

            $this->getManager('pager')
                ->addPage($options);

            $options = [
                'post_title' => __('Register user', 'legato-articles'),
                'post_name' => 'register-user',
                'post_type' => 'page',
                'post_status' => 'publish',
                'post_content' => '[legato-register-user-form]'
            ];

            $this->getManager('pager')
                ->addPage($options);

            $options = [
                'post_title' => __('Login user', 'legato-articles'),
                'post_name' => 'login-user',
                'post_type' => 'page',
                'post_status' => 'publish',
                'post_content' => '[legato-login-user-form]'
            ];

            $this->getManager('pager')
                ->addPage($options);

            return $this;
        }

        /**
         * @param array $slugs
         * @return $this
         */
        private function removePages($slugs = [])
        {
            foreach ($slugs as $slug) {
                $this->getManager('pager')
                    ->removePage($slug);
            }

            return $this;
        }

        /**
         * @return $this
         */
        private function uninstall()
        {
            delete_option('legato_options');

            return $this;
        }

        /**
         * @return $this
         */
        private function menu()
        {
            return $this;
        }

        /**
         * @return $this
         */
        private function paths()
        {
            define('PLUGIN_ASSETS_DIR', plugin_dir_url(__FILE__) . 'assets/');
            define('PLUGIN_ADMINISTRATOR_DIR', plugin_dir_path(__FILE__) . 'administrator/');
            define('PLUGIN_CLASSES_DIR', plugin_dir_path(__FILE__) . 'classes/');
            define('PLUGIN_VIEWS_DIR', plugin_dir_path(__FILE__) . 'views/');
            define('PLUGIN_INC_DIR', plugin_dir_path(__FILE__) . 'inc/');
            define('PLUGIN_LANG_DIR', dirname(plugin_basename(__FILE__)). '/lang/');
            define('PLUGIN_WIDGETS_DIR', plugin_dir_path(__FILE__). '/widgets/');
            define('PLUGIN_VENDOR_DIR', dirname(plugin_basename(__FILE__)). '/vendor/');
            define('PLUGIN_MAILS_DIR', plugin_dir_path(__FILE__) . 'mails/');

            return $this;
        }

        /**
         * @return $this
         */
        public function shortcodes()
        {
            add_shortcode('legato-article-view', function () {
                require_once PLUGIN_VIEWS_DIR . '/_article-view.php';
            });

            add_shortcode('legato-add-article-form', function () {
                require_once PLUGIN_VIEWS_DIR . '/_add-article-form.php';
            });

            add_shortcode('legato-register-user-form', function () {
                require_once PLUGIN_VIEWS_DIR . '/_register-user-form.php';
            });

            add_shortcode('legato-login-user-form', function () {
                require_once PLUGIN_VIEWS_DIR . '/_login-user-form.php';
            });

            add_shortcode('legato-article-view-content', function ($attributes = 0) {
                require_once PLUGIN_VIEWS_DIR . '/_article-view-content.php';
            });

            add_shortcode('iframe', function ($attributes) {

                $attributes = shortcode_atts([
                        'src' => '',
                        'width' => '',
                        'height' => '',
                    ], $attributes, 'iframe');

                return '<iframe frameborder="0" src="'. esc_attr($attributes['src']) .'" width="' . esc_attr($attributes['width']) .'" height="' . esc_attr($attributes['height']) . '"></iframe>';
            });

            return $this;
        }

        /**
         * @return $this
         */
        private function actions()
        {
            $this->paths();

            register_activation_hook(__FILE__, function () {
                $this->install()
                    ->addPages();
            });

            register_deactivation_hook(__FILE__, function () {
                $this->uninstall()
                    ->removePages(['add-article', 'register-user', 'login-user']);
            });

            add_action('init', function () {
                ob_start();

                $this->shortcodes();
                load_plugin_textdomain('legato-articles', false, PLUGIN_LANG_DIR);
            });



            add_action('widgets_init', function() {
                require_once PLUGIN_WIDGETS_DIR . 'AdvertWidget.php';

                register_widget('AdvertWidget');
            });

            add_action('admin_init', function () {
                ob_start();
            });

            add_action('plugins_loaded', function () {

            }, 20);

            if (is_admin()) {
                add_action('admin_menu', function () {
                    $this->menu();
                });
                add_action('admin_enqueue_scripts', function () {
                    $this->enqueue_admin_scripts();
                }, 20);
            }

            add_filter('query_vars', function ($vars) {
                $vars[] = 'category';
                return $vars;
            });

            add_action('wp_enqueue_scripts', function () {
                $this->enqueue_scripts();
            }, 20);

            add_action('form-success', function ($items) {
                echo sprintf('<p class="alert alert-success" role="alert">%s</p>', $items['message']);
            });

            add_action('form-fail', function ($items) {
                echo sprintf('<p class="alert alert-danger" role="alert">%s</p>', $items['message']);
            });

            add_action('publish_post', function ($ID, $post) {

                $author = $post->post_author;

                $username = get_the_author_meta('display_name', $author);

                $email = get_the_author_meta('user_email', $author);

                $title = $post->post_title;

                $permalink = get_permalink($ID);

                $language = get_locale();

                $mail = file_get_contents(PLUGIN_MAILS_DIR . $language . '/_post_published.html');

                $mailMessage = strtr($mail, [
                    '{{ site }}' => get_bloginfo('name'),
                    '{{ name }}' => $username,
                    '{{ title }}' => $title,
                    '{{ link }}' => $permalink
                ]);

                wp_mail($email, __('Post published', 'legato-bank'), $mailMessage);

            }, 10, 2 );

            return $this;
        }

        /**
         * @return $this
         */
        public function enqueue_admin_scripts()
        {
            /* Options script */

            wp_register_style('legato', PLUGIN_ASSETS_DIR . 'css/dist/style.min.css');
            wp_enqueue_style('legato');

            wp_enqueue_script('legato', PLUGIN_ASSETS_DIR . 'js/dist/main.min.js', ['jquery'], '1.0.0', true);

            return $this;
        }

        /**
         * @return $this
         */
        public function enqueue_scripts()
        {
            if (file_exists(get_template_directory_uri() . 'legato/css/style.css')) {
                wp_register_style('legato', get_template_directory_uri() . 'legato/css/style.css');
            } else {
                wp_register_style('legato', PLUGIN_ASSETS_DIR . 'css/dist/style.min.css');
            }

            wp_enqueue_style('legato');

            wp_register_style('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css');
            wp_enqueue_style('jquery-ui');

            wp_enqueue_script('legato', PLUGIN_ASSETS_DIR . 'js/dist/main.min.js', ['jquery'], '1.0.0', true);

            wp_enqueue_script('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js', ['jquery'], '1.9.2');
            wp_enqueue_script('jquery-ui', '', ['jquery-ui'], '1.8.6');

            wp_enqueue_script('tinymce', PLUGIN_ASSETS_DIR . 'vendor/tinymce/tinymce.full.min.js', ['jquery'], '1.0.0', true);

            return $this;
        }

        /**
         *
         */
        public function init()
        {
            $this->actions();
        }

        /**
         * @param $manager
         * @return ArticleManager|bool|CookierManager|FilterManager|PagerManager|PaginationManager|PerpageManager|UserManager|ValidationManager
         */
        public function getManager($manager)
        {
            switch ($manager) {
                case 'article' :
                    return new ArticleManager();
                    break;
                case 'filter' :
                    return new FilterManager();
                    break;
                case 'pager' :
                    return new PagerManager();
                    break;
                case 'cookie' :
                    return new CookierManager();
                    break;
                case 'pagination' :
                    return new PaginationManager();
                    break;
                case 'perpage' :
                    return new PerpageManager();
                    break;
                case 'validation' :
                    return new ValidationManager();
                    break;
                case 'user' :
                    return new UserManager();
                    break;
                default :
                    return false;
            }
        }
    }

    /** @var LegatoArticles $articles */
    $articles = LegatoArticles::getInstance();
    $articles->init();
}
