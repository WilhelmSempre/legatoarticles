(function ($) {

    'use strict';

    var LegatoArticles = function () {

        /**
         *
         */
        this.charsLeft = function () {

            $('[data-characters-left]').each(function() {
                var charactersLeftInput = $(this).find('[data-characters-left-input]'),
                    charactersLeftTinymce = $(this).find('[data-characters-left-tinymce]'),
                    charactersLeftMaxValue = parseInt(charactersLeftInput.attr('max')) || 500,
                    charactersLeftCountHandle = $(this).find('[data-characters-left-value]');

                charactersLeftInput.on('keydown blur', function (event) {
                        event = event || window.event;

                        var charactersCount = charactersLeftInput.val().length,
                        charactersDiffirence = charactersLeftMaxValue - charactersCount;

                    if (charactersDiffirence < 0) {
                        var inputValue = charactersLeftInput.val();

                        charactersLeftInput.val(inputValue.slice(0, charactersLeftMaxValue));
                        charactersDiffirence = 0;

                        event.preventDefault();
                    }

                    charactersLeftCountHandle.text(charactersDiffirence);
                });


                if (charactersLeftTinymce.length !== 0) {
                    var textareaID = charactersLeftTinymce.prop('id'),
                        textareaField = tinyMCE.get(textareaID);

                    textareaField.on('keydown blur', function () {
                        var charactersCount = this.getContent().replace(/(<([^>]+)>)/ig,'').length,
                            charactersDiffirence = charactersLeftMaxValue - charactersCount;

                        if (charactersDiffirence < 0) {
                            var inputValue = charactersLeftInput.val();

                            charactersLeftInput.val(inputValue.slice(0, charactersLeftMaxValue));
                            charactersDiffirence = 0;

                            event.preventDefault();
                        }

                        charactersLeftCountHandle.text(charactersDiffirence);
                    });
                }

                charactersLeftCountHandle.text(charactersLeftMaxValue);
            });

            return this;
        };

        /**
         *
         */
        this.tags = function () {

            var tagsInput =  $('input[data-tags]');

                if (tagsInput.length > 0) {
                    var tagsValue = tagsInput.data('tags');

                    tagsInput.tagit({
                        availableTags: tagsValue.trim().split(', '),
                        autocomplete: {delay: 0, minLength: 2}
                    });
                }

            return this;
        };

        /**
         * @param cname
         * @param cvalue
         * @param exdays
         * @returns {LegatoArticles}
         */
        this.addCookie = function (cname, cvalue, exdays) {
            var data = new Date();
            data.setTime(data.getTime() + (exdays * 24*60*60*1000));
            var expires = "expires="+ data.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";

            return this;
        };

        /**
         *
         */
        this.helpText = function () {
            var helpCover = $('[data-help-cover]'),
                helpCoverClose = $('[data-help-cover-close]'),
                helpCoverShowSwitch = $('[data-help-cover-show-switch]');

            (function (self) {
                helpCoverClose.on('click', function (event) {
                    event = event || window.event;

                    helpCover.fadeOut(500, function () {

                        if (helpCoverShowSwitch.is(':checked')) {
                            self.addCookie('legato-show-cover', 0, 14);
                        }

                        $(this).remove();
                    });

                    event.preventDefault();
                    event.stopPropagation();
                });
            })(this);

            return this;
        };

        /**
         *
         */
        this.init = function () {

            /* Tinymce */
            (function (self) {

                var tinymceLength = $('.tinymce').length,
                    tinymceInstance = 0;

                if (tinymceLength !== 0) {
                    tinymce.init({
                        selector : '.tinymce',
                        plugins: [
                            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                            'searchreplace wordcount visualblocks visualchars code fullscreen',
                            'insertdatetime media nonbreaking save table contextmenu directionality',
                            'emoticons template paste textcolor colorpicker textpattern imagetools codesample youtube'
                        ],
                        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample | youtube',
                        force_br_newlines : true,
                        force_p_newlines : false,
                        editor_encoding: 'raw',
                        extended_valid_elements : '+iframe[src|frameborder|style|scrolling|class|width|height|name|align|allowfullscreen]',
                        setup: function (editor) {
                            editor.on('init', function () {
                                if (tinymceInstance === tinymceLength - 1) {
                                    self.charsLeft();
                                }

                                tinymceInstance++;
                            });
                        }
                    });
                } else {
                    self.charsLeft();
                }

            })(this);

            this.tags()
                .helpText();

            /* IOS Dropdown fix */
            $('body').on('touchstart.dropdown', '.dropdown-menu', function (event) {
                event.stopPropagation();
            });

        };
    };

    $(document).ready(function () {
        new LegatoArticles().init();
    });
})(jQuery);
