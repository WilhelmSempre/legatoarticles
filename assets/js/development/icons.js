(function (document) {
    var container = document.querySelector('.icons');

    if (container) {
    
        container.innerHTML = "<svg xmlns=\"http://www.w3.org/2000/svg\"/>";

    

    } else {
        throw new Error('svginjector: Could not find element: .icons');
    }

})(document);
