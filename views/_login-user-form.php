<?php

$request = array_map(function ($item) {
    return trim(strip_tags($item));
}, $_REQUEST);

$password = $loginormail = '';

/** @var PagerManager $pagerManager */
$pagerManager = $this->getManager('pager');

if (isset($request['user-login-event'])) {

    if (wp_verify_nonce($request['_wpnonce'], 'login-user_'. date('dmy'))) {

        $password = $request['password'];
        $loginormail = $request['login-or-mail'];

        $fields = [
            'password' => $password,
            'login-or-mail' => $loginormail
        ];

        $criterias = [
            'password' => 'required',
            'login-or-mail' => 'required'
        ];


        /** @var ValidationManager $validation */
        $validation = $this->getManager('validation');

        try {
            $validation->setFields($fields)
                ->setCriterium($criterias)
                ->validate();
        } catch (\Exception $error) {
            echo $error->getMessage();
        }

        if (!$validation->isError()) {

            /** @var UserManager $userManager */
            $userManager = $this->getManager('user');

            if (filter_var($loginormail, FILTER_VALIDATE_EMAIL)) {
                $userData = $userManager->getUser(['email' => $loginormail]);
            } else {
                $userData = $userManager->getUser(['login' => $loginormail]);
            }
            if (!$userData) { ?>
                <p class="alert alert-danger" role="alert"><?php _e('User does not exists!', 'legato-articles'); ?></p>
            <?php } else {
                if (wp_hash_password($password) === $userData['password']) { ?>
                    <p class="alert alert-danger" role="alert"><?php _e('Password incorrect', 'legato-articles'); ?></p>
                <?php } else {
                    wp_set_auth_cookie($userData['id'], true, '');
                    do_action('wp_login', $userData['login']);
                    wp_redirect($pagerManager->getPageAddress('add-article'));
                }
            }
        }
    } else {
        do_action('form-fail', [
            'message' => __('Incorrect token!', 'legato-articles')
        ]);
    }
}
?>

<form action="" method="post" class="login-form" novalidate autocomplete="off">
    <label class="col-xs-12 login-form-label control-label">
        <?php _e('Login or email:', 'legato-articles'); ?>
        <?php if (isset($validation) && $validation->isError('login-or-mail')) { ?>
            <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('login-or-mail'); ?></p>
        <?php } ?>
        <input type="text" name="login-or-mail" class="form-control login-form-input" value="<?php echo $loginormail; ?>">
    </label>
    <label class="col-xs-12 login-form-label control-label">
        <?php _e('Password:', 'legato-articles'); ?>
        <?php if (isset($validation) && $validation->isError('password')) { ?>
            <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('password'); ?></p>
        <?php } ?>
        <input type="password" name="password" class="form-control login-form-input">
    </label>
    <label class="col-xs-12 login-form-label control-label">
        <?php wp_nonce_field('login-user_'. date('dmy')); ?>
        <button type="submit" name="user-login-event" class="btn btn-default login-form-submit"><?php _e('Log in!', 'legato-articles'); ?></button>
        <a class="pull-right" href="<?php echo $pagerManager->getPageAddress('register-user'); ?>"><?php _e('You do not have account yet? Sign up!', 'legato-articles'); ?></a>
    </label>
</form>
