<?php

/** @var UserManager $userManager */
$userManager = $this->getManager('user');

/** @var PagerManager $pagerManager */
$pagerManager = $this->getManager('pager');

if (!$userManager->ifUserLogged()) {
    wp_redirect($pagerManager->getPageAddress('login-user'));
}

global $current_user;

array_walk($_POST, function ($item, $key) {
    return ($key === 'title' || $key === 'tags') ? strip_tags($item) : $item;
});

$request = array_merge($_POST, $_GET);

$categoryID = $title = $tags = $content = $advertAddress = $advertTitle = $advertContent = '';
$advert = false;

if (isset($request['add-article-event'])) {

    if (wp_verify_nonce($request['_wpnonce'], 'add-article_'. date('dmy'))) {

        $title = $request['title'];
        $categoryID = $request['category'];
        $tags = $request['tags'];
        $content = stripslashes($request['content']);
        $advertAddress = $request['advert-address'];
        $advertTitle = $request['advert-title'];
        $advert = isset($request['advert']) ? (bool) $request['advert'] : $advert;

        $fields = [
            'title' => $title,
            'tags' => $tags,
            'content' => $content,
            'advert-address' => $advertAddress,
            'advert-title' => $advertTitle,
        ];

        $criterias = [
            'title' => 'required|max:50',
            'tags' => '',
            'content' => 'required',
            'advert-address' => 'max:200',
            'advert-title' => 'max:50',
        ];

        /** @var ValidationManager $validation */
        $validation = $this->getManager('validation');

        try {
            $validation->setFields($fields)
                ->setCriterium($criterias)
                ->validate();
        } catch (\Exception $error) {
            echo $error->getMessage();
        }

        if (!$validation->isError()) {

            $article = [
                'title' => $request['title'],
                'tags'=> $request['tags'],
                'author' => $current_user->ID,
                'category' => $request['category'],
                'content' => $request['content']
            ];

            if (isset($request['advert'])) {

                $article['advert'] = true;
                $article['advert-title'] = $request['advert-title'];
                $article['advert-address'] = $request['advert-address'];
            }

            $articleManager = new ArticleManager();
            $articleSaved = $articleManager->addArticle($article);


            if (!$articleSaved instanceof WP_Error) {
               wp_redirect(add_query_arg('updated', 1, home_url()));
            } else {
                do_action('form-fail', [
                    'message' => $articleSaved->get_error_message()
                ]);
            }
        }
    } else {
        do_action('form-fail', [
            'message' => __('Incorrect token!', 'legato-articles')
        ]);
    }
}

/** @var WP_Term[] $categories */
$categories = get_categories([
    'orderby' => 'name',
    'order' => 'ASC',
    'hide_empty' => 0
]);

$tags = get_tags();
$tagsArray = [];

/** @var WP_Term $tag */
foreach ($tags as $tag) {
    $tagsArray[] = $tag->name;
}

/** @var CookierManager $cookierManager */
$cookierManager = $this->getManager('cookie');
?>

<form action="" method="post" class="legato-add-article-form form-horizontal" novalidate>
    <p class="legato-add-article"><strong><?php _e('User:', 'legato-articles'); ?></strong> <?php echo $current_user->user_login; ?></p>
    <label class="legato-add-article-form-label">
        <?php _e('Choose category:', 'legato-articles'); ?>
        <select class="legato-add-article-form-select" name="category">
            <?php
            foreach ($categories as $category) {
                $selected = $category->term_id === (int) $categoryID ? 'selected' : '';
                echo sprintf('<option %1$s value="%2$s">%3$s</option>', $selected, $category->term_id, esc_html($category->name));
            }
            ?>
        </select>
    </label>
    <label data-characters-left class="legato-add-article-form-label">
        <?php _e('Title:', 'legato-articles'); ?>
        <?php if (isset($validation) && $validation->isError('title')) { ?>
            <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('title'); ?></p>
        <?php } ?>
        <input data-characters-left-input max="50" type="text"
               class="legato-add-article-form-input form-control" name="title" value="<?php echo $title; ?>" required>
        <b>(<?php _e('Characters left:', 'legato-articles'); ?> <span data-characters-left-value class="chars-left"></span>)</b>
    </label>
    <label class="legato-add-article-form-label">
        <?php _e('Tags:', 'legato-articles'); ?>
        <input data-tags="<?php echo implode(', ', $tagsArray); ?>" class="legato-add-article-form-input form-control" type="text" name="tags" value="">
    </label>

    <label data-characters-left class="legato-add-article-form-label add-content">
        <?php _e('Content:', 'legato-articles'); ?>
        <?php if (isset($validation) && $validation->isError('content')) { ?>
            <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('content'); ?></p>
        <?php } ?>
        <?php if (!$cookierManager->exists('legato-show-cover')) { ?>
        <div data-help-cover class="legato-add-article-help">
            <div data-help-cover-text class="legato-add-article-help-text">
                <p class="data-help-cover-text-content">
                    <i><?php _e('Write news with one of topic:', 'legato-articles'); ?><br><br>
                    <b><?php _e('Articles category:', 'legato-articles'); ?></b></i><br>
                </p>

                <?php

                $columns = 4;
                $indicator = 0;

                /** @var WP_Term $category */
                foreach ($categories as $category) { ?>
                    <?php if (($indicator % $columns) === 0) { ?>
                        <ul class="legato-add-article-help-text-list list-unstyled">
                    <?php }

                    ?>
                    <li><u><?php echo $category->name; ?></u></li>

                    <?php
                    $indicator++;

                    if (($indicator % $columns) === 0 ||
                        $indicator === count($categories)) { ?>
                        </ul>
                    <?php }
                }
                ?>

                <p class="data-help-cover-text-content">
                    <i><br><br><?php _e('Write news with one of topic:', 'legato-articles'); ?><br><br>
                        <?php _e('Before article publishing it has been <b>sent to administrators.</b>', 'legato-articles'); ?><br>
                        <?php _e('During publishing you will be informed by e-mail.', 'legato-articles'); ?><br>
                        <?php _e('Before adding read rules.', 'legato-articles'); ?><br>
                        <?php _e('On homepage will be showed 3 lines (180 characters). More after link clicking. Characters limitation is in bottom left border of form.', 'legato-articles'); ?></i>
                </p>
                <div class="legato-add-article-help-text-controls">
                    <button data-help-cover-close class="close-cover btn btn-primary"><?php _e('Understand it!.', 'legato-articles'); ?></button>
                    <label class="show-label">
                        <?php _e('Do not show in future', 'legato-articles'); ?>
                        <input data-help-cover-show-switch name="show" class="show-switch" type="checkbox" value="0">
                    </label>
                </div>
            </div>
        </div>
    <?php } ?>
        <textarea name="content" id="content" class="legato-add-article-form-textarea tinymce form-control"><?php echo stripslashes($content); ?></textarea>
    </label>
    <label for="advert" class="add-advert-button button"><?php _e('Add advert', 'legato-articles'); ?></label>
    <input id="advert" <?php echo ($advert) ? 'checked' : ''; ?> class="legato-add-article-form-advert-switch" name="advert" type="checkbox"
               value="true">

    <aside class="optional legato-add-article-form-advert-sidebar">
        <fieldset class="legato-add-article-form-fieldset">
            <p><b><?php _e('Advert content on homepage and in article:', 'legato-articles'); ?></b></p>
            <label data-characters-left class="legato-add-article-form-label">
                <?php _e('Site address:', 'legato-articles'); ?>
                <?php if (isset($validation) && $validation->isError('advert-address')) { ?>
                    <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('advert-address'); ?></p>
                <?php } ?>
                <input data-characters-left-input max="200" class="legato-add-article-form-input form-control" type="text"
                       name="advert-address" value="<?php echo $advertAddress; ?>">
                <cite><?php _e('Example: http://yoursite.pl', 'legato-articles'); ?></cite><br>
                <b>(<?php _e('Characters left:', 'legato-articles'); ?> <span data-characters-left-value class="chars-left"></span>)</b>
            </label>
            <label data-characters-left class="legato-add-article-form-label">
                <?php _e('Site title:', 'legato-articles'); ?>
                <?php if (isset($validation) && $validation->isError('advert-title')) { ?>
                    <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('advert-title'); ?></p>
                <?php } ?>
                <input data-characters-left-input max="50" class="legato-add-article-form-input form-control" type="text"
                       name="advert-title" value="<?php echo $advertTitle; ?>">
                <b>(<?php _e('Characters left:', 'legato-articles'); ?> <span data-characters-left-value class="chars-left"></span>)</b>
            </label>
        </fieldset>
    </aside>
    <?php wp_nonce_field('add-article_'. date('dmy')); ?>
    <button name="add-article-event" type="submit" value="1"><?php _e('Add article', 'legato-articles'); ?></button>

</form>