<?php

/** @var UserManager $userManager */
$userManager = $this->getManager('user');

if ($userManager->ifUserLogged()) {
    wp_redirect('/');
}

$request = array_map(function ($item) {
    return trim(strip_tags($item));
}, $_REQUEST);

$login = $passwordConfirm = $password = $email = '';

if (isset($request['user-register-event'])) {

    if (wp_verify_nonce($request['_wpnonce'], 'register-user_'. date('dmy'))) {

        $username = $request['username'];
        $password = $request['password'];
        $passwordConfirm = $request['password-confirm'];
        $email = $request['email'];

        $fields = [
            'username' => $username,
            'password' => $password,
            'password-confirm' => $passwordConfirm,
            'email' => $email
        ];

        $criterias = [
            'username' => 'required|alpha_num',
            'password' => 'required|min:6|max:32',
            'password-confirm' => 'required|compare:' . $password,
            'email' => 'required|email'
        ];


        /** @var ValidationManager $validation */
        $validation = $this->getManager('validation');

        try {
            $validation->setFields($fields)
                ->setCriterium($criterias)
                ->validate();
        } catch (\Exception $error) {
            echo $error->getMessage();
        }

        if (!$validation->isError()) {

            $wp_hasher = new PasswordHash(12, true);

            /** @var UserManager $userManager */
            $userManager = $this->getManager('user');

            $userData = [
                'user_pass' => $password,
                'user_login' => $username,
                'user_email' => $email,
                'role' => 'author'
            ];

            /** @var int|WP_Error $user */
            $user = $userManager->addUser($userData);

            if (is_int($user)) { ?>
                <?php
                    wp_redirect(add_query_arg('updated', 2, home_url()));
                ?>
            <?php } else {
                do_action('form-fail', [
                    'message' => $user->get_error_message()
                ]);
            }
        }
    } else {
        do_action('form-fail', [
            'message' => __('Incorrect token!', 'legato-articles')
        ]);
    }
}

?>

<form action="" method="post" class="register-form" novalidate autocomplete="off">
    <label class="col-xs-12 register-form-label control-label">
        <?php _e('Username:', 'legato-articles'); ?>
        <?php if (isset($validation) && $validation->isError('username')) { ?>
            <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('username'); ?></p>
        <?php } ?>
        <input name="username" type="text" class="form-control register-form-input" value="<?php echo $login; ?>">
    </label>
    <label class="col-xs-12 register-form-label control-label">
        <?php _e('Email:', 'legato-articles'); ?>
        <?php if (isset($validation) && $validation->isError('email')) { ?>
            <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('email'); ?></p>
        <?php } ?>
        <input name="email" type="email" class="form-control register-form-input" value="<?php echo $email; ?>">
    </label>
    <label class="col-xs-12 register-form-label control-label">
        <?php _e('Password:', 'legato-articles'); ?>
        <?php if (isset($validation) && $validation->isError('password')) { ?>
            <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('password'); ?></p>
        <?php } ?>
        <input name="password" type="password" class="form-control register-form-input">
    </label>
    <label class="col-xs-12 register-form-label control-label">
        <?php _e('Confirm password:', 'legato-articles'); ?>
        <?php if (isset($validation) && $validation->isError('password-confirm')) { ?>
            <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('password-confirm'); ?></p>
        <?php } ?>
        <input name="password-confirm" type="password" class="form-control register-form-input">
    </label>
    <label class="col-xs-12 register-form-label control-label">
        <?php wp_nonce_field('register-user_'. date('dmy')); ?>
        <input type="hidden" name="action" value="register-user">
        <button type="submit" name="user-register-event"
                class="btn btn-default register-form-submit"><?php _e('Sign in!', 'legato-articles'); ?></button>
    </label>
</form>