<?php

    /** @var ArticleManager $articlesManager */
    $articlesManager = new ArticleManager();

    $options = [
        'p' => (int) $attributes['id']
    ];

    $article = $articlesManager->getArticle($options);
?>

<article class="entry col-xs-12">
    <header class="article-entry-header col-xs-12">
        <?php $visible = count($article[$attributes['id']]['tags']) === 0 ? 'none' : ''; ?>
        <h1 data-tags-list="<?php echo implode(', ', array_keys($article[$attributes['id']]['tags'])); ?>" class="article-header <?php echo $visible; ?>"><?php echo $article[$attributes['id']]['title']; ?></h1>
        <time class="article-time"><?php echo get_the_date(); ?></time>
        <p class="article-author">Napisany przez: <?php echo get_the_author_meta('nicename', $article[$attributes['id']]['author']); ?></p>
    </header>

    <p class="article-entry-content"><?php echo apply_filters('the_content', $article[$attributes['id']]['content']); ?></p>

    <footer class="entry-footer">

    </footer>

</article>
<aside class="comment-sidebar col-xs-12">
    <?php
    if (comments_open() || get_comments_number()) {
        comments_template();
    }

    ?>
</aside>
