<?php

/** @var $request */
$request = array_merge($_POST, $_GET);

/** @var FilterManager $filterManager */
$filterManager = $this->getManager('filter');

/** @var UserManager $userManager */
$userManager = $this->getManager('user');

/** @var PagerManager $pagerManager */
$pagerManager = $this->getManager('pager');

/** @var PerpageManager $perpageManager */
$perpageManager = $this->getManager('perpage');

/** @var PaginationManager $paginationManager */
$paginationManager = $this->getManager('pagination');


if (isset($request['perpage-event'])) {
    $perpageManager->savePerpageValueToCookie($request['perpage']);
}

if (isset($request['add-filter-event'])) {

    $categoryData = null;

    if (isset($request['categories'])) {
        $categoryData = implode(',', $request['categories']);
    }

    $filterManager->saveFilteredCategoriesToCookie($categoryData);
}

$perpageValue = $perpageManager->getPerpageValue();
$filteredCategories = explode(',', $filterManager->getFilterCategories());


/** @var WP_Term[] $categories */
$categories = get_categories([
    'orderby' => 'name',
    'order' => 'ASC',
    'hide_empty' => 0
]);


$postOptions = [
    'posts_per_page' => $perpageValue,
    'offset' => $perpageValue * ($paginationManager->getCurrentPage() - 1)
];

if (is_tag()) {
    global $tag;

    $postOptions['tag'] = $tag;
} else if (is_category()) {
    global $cat;

    $postOptions['cat'] = $cat;
} else {
    $postOptions['cat'] = 9;
}

if ($filterManager->ifFilterCategoriesExists()) {
    $postOptions['cat'] = $filteredCategories;
}

if (isset($request['category'])) {
    $postOptions['cat'] = (int) $request['category'];

}


/** @var ArticleManager $articlesManager */
$articlesManager = new ArticleManager();
$articles = $articlesManager->getArticle($postOptions);

?>
<?php
if (isset($request['updated'])) {
    switch ($request['updated']) {
        case 1:
            do_action('form-success', [
                'message' => sprintf('<b>%s</b><br>%s', __('Article has been added successfully!', 'legato-articles'),
                    __('Article has bee sent to administrators for verification.', 'legato-articles'))
            ]);
            break;
        case 2:
            do_action('form-success', [
                'message' => __('User created!', 'legato-articles')
            ]);
            break;
    }
}
?>
<header class="article-page-header dropdown">
    <h1 class="article-page-header-text dropdown-toggle <?php echo (is_home() && $filteredCategories) ? 'pointer' : ''; ?>" data-toggle="dropdown">
        <?php if (is_tag()) { ?>
            <?php _e('Tag', 'novalite'); ?> : <?php echo get_query_var('tag'); ?>
        <?php } else if (is_category()) { ?>
            <?php _e('Category', 'novalite'); ?> : <?php single_cat_title(); ?>
        <?php } else if (is_month()) { ?>
            <?php _e('Archive for', 'novalite'); ?> : <?php the_time('F, Y'); ?>
        <?php } else { ?>
            Music News
        <?php } ?>
        <?php if (is_home() && $filterManager->ifFilterCategoriesExists()) {
            ?>
                <span class="caret">
        <?php
        }
        ?>
    </h1>
    <?php
    if (is_home() && $filterManager->ifFilterCategoriesExists()) { ?>
    <ul class="dropdown-menu">
        <?php
            foreach ($filteredCategories as $filteredCategory) {

                $categoryName = get_cat_name($filteredCategory);
                $categoryLink = add_query_arg(['category' => $filteredCategory], home_url());
                $categoryEntriesCount = get_category($filteredCategory)->count;
                ?>
                <li><a href="<?php echo $categoryLink; ?>"><?php echo $categoryName; ?> (<?php echo $categoryEntriesCount; ?>)</a></li>
        <?php
            }
        ?>
    </ul>
    <?php } ?>
</header>
<?php if (is_home()) { ?>
    <label for="filter" class="button article-page-filter-button"><?php _e('Filter', 'legato-articles'); ?></label>
    <input type="checkbox" name="filter" class="article-page-filter-checkbox" id="filter" value="true">
<?php } ?>
<aside class="legato-article-view-filter col-xs-12">
    <form action="<?php echo home_url(); ?>" class="table-responsive legato-article-view-filter-form" method="post">
        <table class="legato-article-view-filter-table">
            <?php

                $columns = 3;
                $indicator = 0;

                /** @var WP_Term $category */
                foreach ($categories as $category) { ?>
                    <?php if (($indicator % $columns) === 0) { ?>
                        <tr class="legato-article-view-filter-table-row">
                    <?php }

                    ?>
                                <td class="legato-article-view-filter-table-cell">
                                    <label class="filter-form-label">
                                        <?php echo $category->name; ?>
                                        <?php
                                            $checked = '';
                                            if ($filteredCategories && in_array($category->term_id, $filteredCategories)) {
                                                $checked = 'checked';
                                            }
                                        ?>
                                        <input <?php echo $checked; ?> type="checkbox" class="legato-article-view-filter-form-checkbox" name="categories[]" value="<?php echo $category->term_id; ?>">
                                    </label>
                                </td>


                    <?php
                        $indicator++;

                        if (($indicator % $columns) === 0 ||
                            $indicator === count($categories)) { ?>
                            </tr>
                    <?php }
                }
            ?>
        </table>
        <button type="submit" class="btn btn-primary legato-article-view-filter-form-submit" name="add-filter-event"><?php _e('Save', 'legato-articles'); ?></button>
    </form>
</aside>

<aside class="legato-article-view col-xs-12">
    <?php

    if (count($articles) > 0) {

        foreach ($articles as $article) {

            $categoriesList = [];

            foreach ($article['category'] as $id => $name) {
                $categoriesList[] = sprintf('<a href="%s">%s</a>', add_query_arg(['category' => $id], home_url()), $name);
            }

            ?>
            <article class="article col-xs-12">
                <header class="article-content col-md-8">
                    <div class="article-category col-md-4">
                        <?php $visible = count($article['tags']) === 0 ? 'none' : ''; ?>
                        <p class="article-category-item <?php echo $visible; ?>" data-tags-list="<?php echo implode(', ', array_keys($article['tags'])); ?>"><?php echo implode(',', $categoriesList); ?></p>
                    </div>
                    <div class="article-entry col-md-8">
                        <h4><a title="<?php echo $article['title']; ?>"
                               href="<?php echo get_the_permalink($article['id']); ?>"><?php echo $article['title']; ?></a>
                        </h4>
                        <p>
                            <?php
                            echo wp_trim_words((do_shortcode($article['content'])), 25, '...'); ?>
                        </p>
                        <p class="article-comments-count">
                            <i class="fa fa-comments" aria-hidden="true"></i>
                            <?php
                            $commentsCount = get_comments_number($article['id']);

                            if (comments_open()) {
                                if ($commentsCount == 0) {
                                    $comments = __('No Comments');
                                } elseif ($commentsCount > 1) {
                                    $comments = $commentsCount . __(' Comments');
                                } else {
                                    $comments = __('1 Comment');
                                }
                                $write_comments = '<a href="' . get_comments_link($article['id']) . '">' . $comments . '</a>';
                            } else {
                                $write_comments = __('Comments are off for this post.');
                            }

                            echo $write_comments;
                            ?>
                        </p>
                    </div>
                </header>
                <footer class="article-meta col-md-4">
                    <p class="article-meta-author"><?php echo get_the_author_meta('nicename', $article['author']); ?></p>
                    <?php if (isset($article['advert'])) { ?>
                        <a target="_blank" class="article-meta-advert" href="<?php echo $article['advert-address'] ?>">
                            <strong><?php echo $article['advert-title'] !== '' ? $article['advert-title'] : $article['advert-address'] ?></strong>
                        </a>
                    <?php } ?>
                </footer>
            </article>
            <?php
        }

        $paginationManager->generate($articlesManager->getPostQuery());

    } else {
        get_template_part(get_template_directory_uri() . 'template-parts/content', 'none');
    }
    ?>

</aside>
<aside class="legato-article-view-add-article">
    <?php $link = ($userManager->ifUserLogged()) ? $pagerManager->getPageAddress('add-article') : $pagerManager->getPageAddress('login-user'); ?>
    <a class="button" href="<?php echo $link; ?>"><?php _e('New article', 'legato-articles'); ?></a>
</aside>
<aside class="legato-article-view-perpage">
    <form action="<?php echo home_url(); ?>" method="post" class="legato-article-view-perpage-form">
        <label class="legato-article-view-perpage-form-label">
            <?php __('Articles per page:', 'legato-articles'); ?>
            <?php
            $perpageArray = [5, 10, 25, 50];
            ?>
            <select name="perpage" class="perpage">
                <?php
                foreach ($perpageArray as $perpage) {

                    $selected = '';

                    if ($perpage === $perpageValue) {
                        $selected = 'selected';
                    }
                    ?>
                    <option <?php echo $selected; ?> value="<?php echo $perpage; ?>"><?php echo $perpage; ?></option>
                <?php }
                ?>
            </select>
        </label>
        <button name="perpage-event" class="btn legato-article-view-perpage-form-submit" type="submit"><?php _e('Save', 'legato-articles'); ?></button>
    </form>
</aside>