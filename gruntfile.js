module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            build: {
                src: 'assets/js/development/main.js',
                dest: 'assets/js/dist/main.min.js'
            }
        },
        jshint: {
            all: {
                src: ['assets/js/development/scripts.js']
            }
        },
        concat: {
            options: {
                separator: ';',
                processImport: false
            },
            dist: {
                src: ['assets/js/development/bower.js', 'assets/js/development/scripts.js'],
                dest: 'assets/js/development/main.js'
            }
        },
        cssmin: {
            target: {
                files: {
                    'assets/css/dist/style.min.css': [
                        'assets/css/development/*.css'
                    ]
                }
            }
        },
        /* sass: {
         dist: {
         options: {
         style: 'expanded',
         noCache: true
         },
         files: [{
         expand: true,
         cwd: 'assets/scss',
         src: ['*.scss'],
         dest: 'assets/css/development',
         ext: '.css'
         }]
         }
         }, */
        // svgstore: {
        //     options: {
        //         prefix: 'shape-',
        //         convertNameToId: function(name) {
        //             return name.replace(/^\w+\_/, '');
        //         },
        //         cleanup: true,
        //         cleanupdefs: true
        //     },
        //     default: {
        //         files: {
        //             'assets/img/icons/dist/icons.svg': ['assets/img/icons/*.svg']
        //         }
        //     }
        // },
        // svginjector: {
        //     icons: {
        //         files: {
        //             'assets/js/development/icons.js': ['assets/img/icons/dist/icons.svg']
        //         },
        //         options: {
        //             container: '.icons'
        //         }
        //     }
        // },
        bower_concat: {
            all: {
                options: {
                    separator: ';'
                },
                dest: {
                    'js': 'assets/js/development/bower.js',
                    'css': 'assets/css/development/bower.css'
                },
                mainFiles: {
                    'tag-it' : ['js/tag-it.min.js', 'css/jquery.tagit.css', 'css/tagit-ui-zendesk.css'],
                    'bootstrap' : ['dist/css/bootstrap.css', 'dist/js/bootstrap.min.js']
                },
                bowerOptions: {
                    relative: false
                }
            }
        },
        postcss: {
            options: {
                map: true,
                processors: [
                    require('autoprefixer')({
                        browsers: ["last 10 version", "> 1%", "ie 8", "ie 7"]
                    })
                ]
            },
            dist: {
                src: 'assets/css/development/style.css'
            }
        },
        watch: {
            scripts: {
                files: [
                    'gruntfile.js',
                    'assets/js/development/*.js',
                    'assets/css/development/*.css',
                    'package.json'],
                //'assets/scss/*.scss'],
                tasks: [
                    'jshint',
                    'bower_concat',
                    //'sass',
                    // 'svgstore',
                    // 'svginjector',
                    'concat',
                    'uglify',
                    'postcss',
                    'cssmin'
                ]
            },
            options: {
                dateFormat: function (time) {
                    grunt.log.writeln('Monitorowanie zakończone w ciągu: ' + time + 'ms o godzinie: ' + (new Date()).toString());
                    grunt.log.writeln('Monitorowanie plików');
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    //grunt.loadNpmTasks('grunt-contrib-sass');
    // grunt.loadNpmTasks('grunt-svgstore');
    grunt.loadNpmTasks('grunt-bower-concat');
    // grunt.loadNpmTasks('grunt-svginjector');
    grunt.loadNpmTasks('grunt-postcss');

    grunt.registerTask('default', [
        'jshint',
        'bower_concat',
        //'sass',
        // 'svgstore',
        // 'svginjector',
        'concat',
        'uglify',
        'postcss',
        'cssmin'
    ]);
};